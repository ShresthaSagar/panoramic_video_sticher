#include "controlpoints.h"

void ControlPoints::stackImage(Mat image1, Mat image2, Mat &stackedOutput)
{
//This module is for stack two images horizontally
    int col = image1.cols + image2.cols;
    int row = image2.rows;

    if (image1.rows>row)
    {
        row = image1.rows;
    }

    //Create a blank image
    Mat temp(row, col, CV_8UC3, Scalar(0, 000, 00));

    image1.copyTo(temp(Rect(0,0,image1.cols, image1.rows)));
    image2.copyTo(temp(Rect(image1.cols, 0, image2.cols, image2.rows)));

    stackedOutput = temp;
}

void ControlPoints::parsePoint( int event, int x, int y, int d, void *ptr )
{
    int SQUARE = 20;
    int scale= 5;

    data *d1 = (data*) ptr;

    Mat temp;
    d1->stackedImage.copyTo(temp);

    if(event == EVENT_LBUTTONDOWN)
    {
        //Point lies on right image
        if (x > d1->leftImage.cols)
        {
            x = x - d1->leftImage.cols;
            cout<<endl<<"Image 2 "<<x<<" "<<y;

            d1->rightPoint.push_back(Point2f(x,y));
//            cout<<endl<<"Size of right point : "<<d1->rightPoint.size();
//            cout<<endl<<"Size of left point : "<<d1->leftPoint.size();
            x = x + d1->leftImage.cols;
        }

        else    //Point lies on left image
        {
            cout<<endl<<"Image 1 "<<x<<" "<<y;
            d1->leftPoint.push_back(Point2f(x,y));
        }

        circle(d1->stackedImage, Point(x,y), 10, Scalar(0,0,0255), 1);
    }

    int startX = (float)x-((float)SQUARE/2.0);
    int startY = (float)y-((float)SQUARE/2.0);

    if((x>SQUARE/2.0) && (y > SQUARE /2.0) && (x<(d1->stackedImage.cols-(SQUARE)) && (y<(d1->stackedImage.rows-(SQUARE)))))
    {
        //Roi - to show the keypoint accurately by zooming
        Mat roi = temp(Rect(startX, startY, SQUARE, SQUARE));
        resize(roi, roi, Size(SQUARE*5,SQUARE*5));
        line(roi, Point(0,roi.rows/2),Point(roi.cols,roi.rows/2),Scalar(255,255,255),3);
        line(roi, Point(roi.cols/2,0), Point(roi.cols/2,roi.rows),Scalar(255,255,255),3);
        roi.copyTo(d1->stackedImage(Rect(0,0,roi.cols, roi.rows)));
    }

    // Criss-cross to show the selected point accurately
    line(temp, Point(x,0), Point(x, temp.rows), Scalar(255,255,255),3);
    line(temp, Point(0,y), Point(temp.cols,y), Scalar(255,255,255),3);
    imshow("Set Control Points", temp);
}


void ControlPoints::setControlPoints(Mat image1, Mat image2, vector<Point2f> &srcPoints, vector<Point2f> &dstPoints)
{
    namedWindow("Set Control Points", WINDOW_NORMAL);
    Mat stacked;
    stackImage(image1, image2, stacked);

    vector<Point2f> tempLeftPoint;
    vector<Point2f> tempRighPoint;

    data d1, *d1Ptr;

    d1.leftImage = image1;
    d1.rightImage = image2;
    d1.stackedImage = stacked;
    d1.leftPoint = tempLeftPoint;
    d1.rightPoint = tempRighPoint;

    d1Ptr = &d1;
    setMouseCallback("Set Control Points", parsePoint, d1Ptr);

    while(true)
    {
        imshow("Set Control Points", d1.stackedImage);
        int k = waitKey(20);
        if(k == 27)
        {
            destroyAllWindows();
            break;
        }
    }
//    cout<<endl<<"No of left points : "<<d1.leftPoint.size();
//    cout<<endl<<"No of right points : "<<d1.leftPoint.size();
    srcPoints = d1.leftPoint;
    dstPoints = d1.rightPoint;
}

void ControlPoints::applyControlPoints(vector<ImageFeatures> &features, vector<MatchesInfo> &pairwise_matches, vector<Mat> &images, const UMat &matcher_mask_, const double & conf_thresh_)
{
    vector<Point2f> pointsi;
    vector<Point2f> pointsj;

    const int num_images = static_cast<int>(features.size());
    Mat_<uchar> mask_(matcher_mask_.getMat(ACCESS_READ));
    for(int i=0; i<features.size(); i++)
    {
        for(int j=i+1; j<features.size(); j++)
        {
            int match_idx =i*num_images+j;
            if(mask_(i,j) &&(pairwise_matches[match_idx].confidence < conf_thresh_))
            {
                pointsi.clear();
                pointsj.clear();
                setControlPoints(images[i], images[j], pointsi, pointsj);
                CV_Assert(pointsi.size()==pointsj.size());

                int sizei = features[i].keypoints.size();
                int sizej = features[j].keypoints.size();

                for(int k=0;k<pointsi.size();k++)
                {
                    cout<<endl<<pointsi[k]<<"\t"<<pointsj[k];
                    features[i].keypoints.push_back(KeyPoint(pointsi[k], 1.f));
                    features[j].keypoints.push_back(KeyPoint(pointsj[k], 1.f));
                    pairwise_matches[match_idx].matches.push_back(DMatch(sizei+k, sizej+k, 0));
                    pairwise_matches[match_idx].inliers_mask.push_back(1);
                    pairwise_matches[match_idx].num_inliers++;
                }
                pairwise_matches[match_idx].confidence = pairwise_matches[match_idx].num_inliers / (8 + 0.3 * pairwise_matches[match_idx].matches.size());

                // Construct point-point correspondences for homography estimation
                Mat src_points(1, static_cast<int>(pairwise_matches[match_idx].matches.size()), CV_32FC2);
                Mat dst_points(1, static_cast<int>(pairwise_matches[match_idx].matches.size()), CV_32FC2);
                for (size_t k = 0; k < pairwise_matches[match_idx].matches.size(); ++k)
                {
                    const DMatch& m = pairwise_matches[match_idx].matches[k];

                    Point2f p = features[i].keypoints[m.queryIdx].pt;
                    p.x -= features[i].img_size.width * 0.5f;
                    p.y -= features[i].img_size.height * 0.5f;
                    src_points.at<Point2f>(0, static_cast<int>(k)) = p;

                    p = features[j].keypoints[m.trainIdx].pt;
                    p.x -= features[j].img_size.width * 0.5f;
                    p.y -= features[j].img_size.height * 0.5f;
                    dst_points.at<Point2f>(0, static_cast<int>(k)) = p;
                }

                // Find pair-wise motion
                pairwise_matches[match_idx].H = findHomography(src_points, dst_points, 0);

            }
        }
    }
}
