#!/bin/sh

n=0;
echo "transforming masks"

cd $1
mkdir $2;
mkdir TempImages;
for i in *.jpg
do
  n=$((n+1))
  convert $i -virtual-pixel black -distort Barrel "0.01 0.0 -0.43 1.7" TempImages/$i
  ffmpeg -i TempImages/$i -vf "crop=1920:766:0:157" $2/$i 
  echo "transformed Image $n"
done

