#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/stitching/detail/matchers.hpp>

#include <cctype>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include<iostream>

using namespace cv;
using namespace cv::detail;
using namespace std;
//string winName = "Set Control Points";
class ControlPoints{

public:
    struct data{
        Mat leftImage;
        Mat rightImage;
        Mat stackedImage;
        vector<Point2f> leftPoint;
        vector<Point2f> rightPoint;
    };
    void stackImage(Mat image1, Mat image2, Mat &stackedOutput);
    static void parsePoint( int event, int x, int y, int d, void *ptr );
    void setControlPoints(Mat image1, Mat image2, vector<Point2f> &srcPoints, vector<Point2f> &dstPoints);
    void applyControlPoints(vector<ImageFeatures> &features, vector<MatchesInfo> &pairwise_matches, vector<Mat> &images, const UMat &matcher_mask_, const double &conf_thresh_);

};



/*
int main(int argc, char** argv)
{
    // Read image from file
    Mat image1 =  imread("/home/paaila/Desktop/360video/ControlPoints/Img01.jpg");;
    Mat image2 =  imread("/home/paaila/Desktop/360video/ControlPoints/Img01.jpg");;

//    //if fail to read the image
//    if ( image1.empty() || image2.empty() )
//    {
//        cout << "Error loading the image" << endl;
//        return -1;
//    }

    vector<Point2f> srcPoints, dstPoints;
    setControlPoints(image1, image2, srcPoints, dstPoints);

    cout<<endl<<"No of left points : "<<srcPoints.size();
    cout<<endl<<"No of right points : "<<dstPoints.size();

    for(int i=0;i<srcPoints.size();i++)
    {
        cout<<endl<<srcPoints[i]<<"\t"<<dstPoints[i];
    }

    return 0;
}
*/

